module.exports = Object.freeze({
   filePath : './README.md',
   error    : 'An error occured. \n',

   apiRequest   : "https://api.bitbucket.org/2.0/repositories/tizdev/template_readme_git/commits/master",
   bitbucketRaw : "https://bitbucket.org/tizdev/template_readme_git/raw/",
});