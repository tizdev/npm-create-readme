module.exports.StringService = class StringService {

   /** Remove the accents **/
   removeAccents(string){
      return string.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
   }

   /* Return the string with the first letter in uppercase */
   uppercaseFirstChar(string){
      return string.charAt(0).toUpperCase() + string.slice(1);
   }
}
