//----------------------------------------- //
//--------------- Imports  ---------------- //
//----------------------------------------- //

const fetch    = require("node-fetch");
const fs       = require('fs');
const readline = require('readline');

const constants        = require('./common/constants');
const strServiceImport = require('./services/stringService')
const stringService = new strServiceImport.StringService();

// Create readline interface
const rl = readline.createInterface({ 
   input  : process.stdin, 
   output : process.stdout,
}); 

//----------------------------------------- //
//--------------- Functions --------------- //
//----------------------------------------- //

/** Get the specified Readme **/
function createReadme(technoTyped) {
   return fetch(constants.apiRequest).then(function(response) {
      response.json().then(function(commits) {
         let hash = commits['values'][0]['hash'];

         // Remove diatrics and uppercase from key
         let technoTypedFormatted = stringService.removeAccents(technoTyped.toString().toLowerCase());

         let techURL;
         if(technoTypedFormatted == "standard") techURL = constants.bitbucketRaw+hash+"/README.md";
         else techURL = constants.bitbucketRaw+hash+"/"+stringService.uppercaseFirstChar(technoTypedFormatted)+"/README_"+technoTypedFormatted+".md";

         fetchURL(hash, techURL, technoTypedFormatted);
      });
   });
}


/** Fetch the URL depending the key **/
function fetchURL(hash, techURL, technoTypedFormatted) {
   return fetch(techURL).then(function(response) {

      // Key is not assigned to any folder on bitbucket READMEs
      if(response.status == 404){
         console.log("\nKey not recognized. \nAvailable : drupal / flutter / laravel / prestashop / symfony / reactjs / wordpress / standard \n");
         console.log("------\nTry : 'readme drupal'\n------\n");
        
         // Ask if user want a standard file
         rl.question('Do you want to use standard README [yes/NO] ? ', (answer) => {
            if (answer.match(/^y(es)?$/i)){
               techURL = constants.bitbucketRaw+hash+"/README.md";
               technoTypedFormatted = "standard";
               fetchURL(hash, techURL, technoTypedFormatted);
            }else{
               console.log("\nREADME creation cancelled.\n");
               rl.close();
            }
         });

      // Key is found on bitbucket READMEs
      }else{

         // Check if README already exist
         response.text().then(function(text) {
            fs.access(constants.filePath, fs.constants.F_OK, (notExist) => {

               // 1. Not exist : just write the file
               if (notExist) {
                  saveFile(technoTypedFormatted, text);

               // 2. Exist : Ask if user want to erase existing file
               }else{
                  rl.question('\nREADME file already exist. Do you want to erase it [yes/NO] ? ', (answer) => {
   
                     // Erase
                     if (answer.match(/^y(es)?$/i)){
                        saveFile(technoTypedFormatted, text);

                     }else{
                        console.log("\nREADME creation cancelled.\n");
                        rl.close();
                     }
                  });
               }
            });
         });
      }
   }).catch(err => {
      console.error(constants.error+" => ", err);
      rl.close();
   });
}


/** Save the readme **/
function saveFile(technoTyped, text) {
   fs.writeFile(constants.filePath, text, (err) => {
      if(err){
         console.error(constants.error+" => ", err);
         throw err;
      }else{
         console.log("\nThe "+technoTyped+" README has been saved. \n");
      } 
      rl.close();
   }); 
}

module.exports = createReadme;
